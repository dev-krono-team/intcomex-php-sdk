<?php

use Kronoapp\Service\CurlService;
use PHPUnit\Framework\TestCase;

class CurlServiceTest extends TestCase
{

    protected $curlService;

    public function setUp()
    {
        $this->curlService = new CurlService();
    }

    public function testSetUri()
    {
        $expectedResult = 'http://localhost/test?name=Camilo&lastname=Martinez';
        $actualResult = invokeMethod($this->curlService, 'setUri', [
           'http://localhost/test',
           ['name' => 'Camilo', 'lastname' => 'Martinez']
        ]);
        $this->assertEquals($expectedResult, $actualResult);
    }

    public function testGet()
    {
        $actualResult = $this->curlService->get('http://localhost/test?name=Camilo&lastname=Martinez');
        $this->assertEmpty($actualResult);
    }

    public function testGetHttpStatusCode()
    {
        $this->curlService->get('http://localhost/test?name=Camilo&lastname=Martinez');
        $this->assertTrue(404 === $this->curlService->getHttpStatusCode());
    }
}