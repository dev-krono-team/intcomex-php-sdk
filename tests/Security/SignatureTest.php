<?php


use Kronoapp\Security\Signature;
use PHPUnit\Framework\TestCase;

class SignatureTest extends TestCase
{
    public function testGenerateSignature()
    {
        $apiKey = '12345678';
        $accessKey = 'Kr0n04pp-12345678';
        $timestamp = (new DateTime())->format('Y-m-d\TH:i:s\Z');
        $signature = new Signature($apiKey, $accessKey, new DateTime());
        $actualResult = $signature->generateSignature();
        $expectedResult = hash('sha256', sprintf('%s,%s,%s', $apiKey,$accessKey,$timestamp));
        $this->assertEquals($actualResult, $expectedResult);
    }
}
