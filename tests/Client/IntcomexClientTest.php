<?php


use Kronoapp\Client\IntcomexClient;
use Kronoapp\Contracts\HttpServiceInterface;
use Kronoapp\Contracts\SignatureInterface;
use PHPUnit\Framework\TestCase;


class IntcomexClientTest extends TestCase
{
    protected $client;

    public function setUp() {
        $signature = $this->createMock(SignatureInterface::class);
        $service = $this->createMock(HttpServiceInterface::class);
        $this->client = new IntcomexClient($signature, $service);
    }

    public function testGetEnv()
    {
        $env = $this->client->getEnv();
        $expectedUri = $this->client::BASE_URL[$env];
        $this->assertEquals('DEV', $this->client->getEnv());
        $this->assertEquals('https://intcomex-test.apigee.net/v1/', $expectedUri);
    }

}
