<?php


namespace Kronoapp\Contracts;


interface SignatureInterface
{
    const ALGORITHM = 'sha256';

    /**
     * Generate a signing for all request
     *
     * @return string
     */
    public function generateSignature();

    /**
     * Return timestamp with UTC format
     *
     * @return string
     */
    public function getTimestamp();

    /**
     * Return Intcomex user public key
     *
     * @return string
     */
    public function getApiKey();

    /**
     * Return Intcomex user private key
     *
     * @return string
     */
    public function getAccessKey();
}