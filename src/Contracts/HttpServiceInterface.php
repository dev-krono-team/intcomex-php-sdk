<?php


namespace Kronoapp\Contracts;


interface HttpServiceInterface
{

    /**
     * Send a get request method
     *
     * @param string $uri Http resource identifier
     * @param array $params [optional] Uri params
     * @return array
     */
    public function get(string $uri, array $params = []);

    /**
     * Send a post request method
     *
     * @param string $uri Http resource identifier
     * @param array $params [optional] Uri params
     * @param mixed $content [optional] Request data
     * @return array
     */
    public function post(string $uri, array $params = [], $content = null);

    /**
     * Send a put request method
     *
     * @param string $uri Http resource identifier
     * @param array $params [optional] Uri params
     * @param mixed $content [optional] Request data
     * @return array
     */
    public function put(string $uri, array $params = [], $content = null);

    /**
     * Send a delete request method
     *
     * @param string $uri Http resource identifier
     * @param array $params [optional] Uri params
     * @return array
     */
    public function delete(string $uri, array $params = []);

    /**
     * Return status code response of the request
     *
     * @return int
     */
    public function getHttpStatusCode();

    /**
     * Set request headers
     *
     * @param array $httpHeaders Array of headers for request
     * @return mixed
     */
    public function setHttpHeaders(array $httpHeaders);
}