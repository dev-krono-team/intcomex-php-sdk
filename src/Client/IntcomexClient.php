<?php


namespace Kronoapp\Client;


use DateTimeInterface;
use Kronoapp\Contracts\HttpServiceInterface;
use Kronoapp\Contracts\SignatureInterface;

class IntcomexClient
{
    /**
     * @var SignatureInterface
     */
    protected $signature;

    /**
     * @var HttpServiceInterface
     */
    protected $service;

    /**
     * Http request parameters
     *
     * @var array
     */
    protected $params = [];

    /**
     * Define work environment for client
     * By default set Development environment
     *
     * @var string
     */
    private $env;

    /**
     * Environments available
     */
    const BASE_URL = [
        'DEV' => 'https://intcomex-test.apigee.net/v1/',
//        'PROD' => 'https://intcomex-prod.apigee.net/v1/'
        'PROD' => 'https://iws.intcomex.com/api/iwsapi/'
    ];

    /**
     * IntcomexClient constructor.
     *
     * @param SignatureInterface $signature
     * @param HttpServiceInterface $service
     * @param string $env [optional]
     */
    public function __construct(SignatureInterface $signature, HttpServiceInterface $service, $env = 'DEV')
    {
        $this->env = $env;
        $this->signature = $signature;
        $this->service = $service;
        $this->params = [
            'apiKey' => $this->signature->getApiKey(),
            'utcTimestamp' => $this->signature->getTimestamp(),
            'signature' => $this->signature->generateSignature()
        ];
    }

    /**
     * Retrieve a http service bound to client
     *
     * @return HttpServiceInterface
     */
    public function getService(): HttpServiceInterface
    {
        return $this->service;
    }

    /**
     * Return actual environment
     *
     * @return string
     */
    public function getEnv(): string
    {
        return $this->env;
    }

    /**
     * Get a full catalog of products.
     *
     * @param string $locale Determines the regional settings to use in the results
     * @param $params [optional] Optional parameters
     * @return array
     */
    public function getCatalog(string $locale, string ...$params): array
    {
        $action = strtolower(__FUNCTION__);
        $this->params['locale'] = $locale;
        $this->setAdditionalParams($params);
        return $this->service->get(self::BASE_URL[$this->env] . $action, $this->params);
    }

    /**
     * Get prices and stock availability for the catalog of products.
     *
     * @return array
     */
    public function getCatalogSalesData(): array
    {
        $action = strtolower(__FUNCTION__);
        return $this->service->get(self::BASE_URL[$this->env] . $action, $this->params);
    }

    /**
     * Get a custom list of products
     *
     * @param string $locale Determines the regional settings to use in the results
     * @param string $skusList A comma separated list of Intcomex SKU.
     * @param $params [optional] Optional parameters
     * @return array
     */
    public function getProducts(string $locale, string $skusList, string ...$params): array
    {
        $action = strtolower(__FUNCTION__);
        $this->params['locale'] = $locale;
        $this->params['skusList'] = $skusList;
        $this->setAdditionalParams($params);
        return $this->service->get(self::BASE_URL[$this->env] . $action, $this->params);
    }

    /**
     * Get a single product.
     *
     * @param string $locale Determines the regional settings to use in the results
     * @param string $sku The Intcomex SKU of the product to request.
     * @param $params [optional] Optional parameters
     * @return array
     */
    public function getProduct(string $locale, string $sku, string ...$params): array
    {
        $action = strtolower(__FUNCTION__);
        $this->params['locale'] = $locale;
        $this->params['sku'] = $sku;
        $this->setAdditionalParams($params);
        return $this->service->get(self::BASE_URL[$this->env] . $action, $this->params);
    }

    /**
     * Get prices and stock availability for a single product.
     *
     * @param string $sku The Intcomex SKU of the product to request.
     * @param bool $includePriceData [optional] Determine whether to include price data for the product in the response.
     * @return array
     */
    public function getProductSalesData(string $sku, bool $includePriceData = true): array
    {
        $action = strtolower(__FUNCTION__);
        $this->params['sku'] = $sku;
        $this->params['includePriceData'] = $includePriceData;
        return $this->service->get(self::BASE_URL[$this->env] . $action, $this->params);
    }

    /**
     * Place an Intcomex order
     *
     * @param array $payload Data for create resource
     * @param $params [optional] Optional parameters
     * @return array
     */
    public function placeOrder(array $payload, string ...$params): array
    {
        $action = strtolower(__FUNCTION__);
        $this->setAdditionalParams($params);
        return $this->service->post(self::BASE_URL[$this->env] . $action, $this->params, json_encode($payload));
    }

    /**
     * Get an Intcomex order.
     *
     * @param string $orderNumber The Intcomex order number to obtain.
     * @return array
     */
    public function getOrder(string $orderNumber): array
    {
        $action = strtolower(__FUNCTION__);
        $this->params['orderNumber'] = $orderNumber;
        return $this->service->get(self::BASE_URL[$this->env] . $action, $this->params);
    }

    /**
     * The GetOrders API can be used to obtain a list of orders based on certain parameters.
     *
     * @param DateTimeInterface|null $startDate
     * @param DateTimeInterface|null $endDate
     * @param int|null $statusCode
     * @param string|null $tag
     * @return array
     */
    public function getOrders(
        DateTimeInterface $startDate = null,
        DateTimeInterface $endDate = null,
        int $statusCode = null,
        string $tag = null
    ) {
        $action = strtolower(__FUNCTION__);
        if (!is_null($startDate)) {
            $this->params['startDate'] = $startDate->format('Y-m-d');
        }
        if (!is_null($endDate)) {
            $this->params['endDate'] = $endDate->format('Y-m-d');
        }
        if (!is_null($statusCode)) {
            $this->params['statusCode'] = $statusCode;
        }
        if (!is_null($tag)) {
            $this->params['tag'] = $tag;
        }
        return $this->service->get(self::BASE_URL[$this->env] . $action, $this->params);
    }

    /**
     * Register one or more payments for an order.
     *
     * @param array $payload Request data
     * @return array
     */
    public function registerPayments(array $payload): array
    {
        $action = strtolower(__FUNCTION__);
        return $this->service->post(self::BASE_URL[$this->env] . $action, $this->params, json_encode($payload));
    }

    /**
     * Release an order for billing and shipping.
     *
     * @param array $payload Request data
     * @return array
     */
    public function releaseOrder(array $payload): array
    {
        $action = strtolower(__FUNCTION__);
        return $this->service->post(self::BASE_URL[$this->env] . $action, $this->params, json_encode($payload));
    }

    /**
     * The CancelOrder API can cancel a placed order, as long as it has not been shipped yet.
     *
     * @param array $payload Request data
     * @return array
     */
    public function cancelOrder(array $payload): array
    {
        $action = strtolower(__FUNCTION__);
        return $this->service->post(self::BASE_URL[$this->env] . $action, $this->params, json_encode($payload));
    }

    /**
     * Update the shipping information for an order.
     *
     * @deprecated
     * @param array $payload Request data
     * @return array
     */
    public function updateOrderShipping(array $payload): array
    {
        $action = strtolower(__FUNCTION__);
        return $this->service->post(self::BASE_URL[$this->env] . $action, $this->params, json_encode($payload));
    }

    /**
     * The UpdateOrder API allows updating certain information on an order after it has been placed.
     *
     * @param array $payload
     * @return array
     */
    public function updateOrder(array $payload): array
    {
        $action = strtolower(__FUNCTION__);
        return $this->service->post(self::BASE_URL[$this->env] . $action, $this->params, json_encode($payload));
    }

    /**
     * The GetPlaces API can be used to obtain a list of geographic places for a given country and optional parent place.
     *
     * @param int|null $parentId
     * @param string $countryId
     * @return array
     */
    public function getPlaces(int $parentId = null, string $countryId = 'MX')
    {
        $action = strtolower(__FUNCTION__);
        $this->params['countryId'] = $countryId;
        if (!is_null($parentId)) {
            $this->params['parentId'] = $parentId;
        }
        return $this->service->get(self::BASE_URL[$this->env] . $action, $this->params);
    }

    /**
     * Get an invoice.
     *
     * @param string $invoiceNumber The invoice number to obtain.
     * @return array
     */
    public function getInvoice(string $invoiceNumber): array
    {
        $action = strtolower(__FUNCTION__);
        $this->params['invoiceNumber'] = $invoiceNumber;
        return $this->service->get(self::BASE_URL[$this->env] . $action, $this->params);
    }

    /**
     * Add optional params to request
     *
     * @param array $params
     */
    private function setAdditionalParams(array $params)
    {
        foreach ($params as $param) {
            if (strpos($param, ':') !== false) {
                $param = explode(':', $param);
                $key = $param[0];
                $value = $param[1];
                $this->params[$key] = $value;
            }
        }
    }

}