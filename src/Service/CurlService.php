<?php


namespace Kronoapp\Service;


use Kronoapp\Contracts\HttpServiceInterface;
use Exception;

class CurlService implements HttpServiceInterface
{
    /**
     * Response status code
     *
     * @var int
     */
    private $httpStatusCode;

    /**
     * Request Http headers
     *
     * @var array
     */
    private $httpHeaders;

    /**
     * @var string
     */
    private $uri;

    /**
     * CurlService constructor.
     * @param array $httpHeaders
     * @throws Exception
     */
    public function __construct(array $httpHeaders = [])
    {
        $this->httpHeaders = $httpHeaders;
    }

    /**
     * {@inheritDoc}
     */
    public function get(string $uri, array $params = [])
    {
        return $this->sendRequest($uri, 'GET', $params);
    }

    /**
     * {@inheritDoc}
     */
    public function post(string $uri, array $params = [], $content = null)
    {
        return $this->sendRequest($uri, 'POST', $params, $content);
    }

    /**
     * {@inheritDoc}
     */
    public function put(string $uri, array $params = [], $content = null)
    {
        return $this->sendRequest($uri, 'PUT', $params, $content);
    }

    /**
     * {@inheritDoc}
     */
    public function delete(string $uri, array $params = [])
    {
        return $this->sendRequest($uri, 'DELETE', $params);
    }

    /**
     * {@inheritDoc}
     */
    public function getHttpStatusCode()
    {
        return $this->httpStatusCode;
    }

    /**
     * {@inheritDoc}
     */
    public function setHttpHeaders(array $httpHeaders)
    {
        $this->httpHeaders = $httpHeaders;
    }

    /**
     * Execute Request and return a response
     *
     * @param string $uri Resource http identifier
     * @param string|null $method [optional] Http verb
     * @param array $params [optional] Uri parameters
     * @param mixed $content [optional] Request data
     * @return mixed|array
     */
    private function sendRequest(string $uri, string $method = null, array $params = [], $content = null)
    {
        $ch = curl_init($this->setUri($uri, $params));
        $opts = [
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_RETURNTRANSFER => 1
        ];
        if (!is_null($method)) {
            $opts[CURLOPT_CUSTOMREQUEST] = $method;
        }
        if (!empty($content)) {
            $opts[CURLOPT_POSTFIELDS] = $content;
        }
        if (!empty($this->httpHeaders)) {
            $opts[CURLOPT_HTTPHEADER] = $this->httpHeaders;
        }
        curl_setopt_array($ch, $opts);
        $response = json_decode(curl_exec($ch), true);
        $this->setHttpStatusCode(curl_getinfo($ch, CURLINFO_HTTP_CODE));
        curl_close($ch);
        return $response ?? [];
    }

    /**
     * Return a valid uri for request
     *
     * @param string $uri
     * @param array $params
     * @return string
     */
    private function setUri(string $uri, array $params = []): string
    {
        $this->uri = $uri;
        if (!empty($params)) {
            $paramsJoined = [];
            foreach ($params as $key => $value) {
                $paramsJoined[] = "$key=$value";
            }
            $this->uri .= '?' . implode('&', $paramsJoined);
        }
        return $this->uri;
    }

    /**
     * Define http status code to request
     *
     * @param int $httpStatusCode
     */
    private function setHttpStatusCode(int $httpStatusCode)
    {
        $this->httpStatusCode = $httpStatusCode;
    }
}