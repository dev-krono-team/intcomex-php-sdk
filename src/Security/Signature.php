<?php


namespace Kronoapp\Security;

use DateTimeInterface;
use Kronoapp\Contracts\SignatureInterface;

final class Signature implements SignatureInterface
{
    /**
     * UTC timestamp
     *
     * @var string
     */
    private $timestamp;

    /**
     * User public key
     *
     * @var string
     */
    private $apiKey;

    /**
     * User private key
     *
     * @var string
     */
    private $accessKey;

    /**
     * Signature constructor.
     * @param string $apiKey
     * @param string $accessKey
     * @param DateTimeInterface $dateTime
     */
    public function __construct(string $apiKey, string $accessKey, DateTimeInterface $dateTime)
    {
        $this->timestamp = $dateTime->format('Y-m-d\TH:i:s\Z');
        $this->apiKey = $apiKey;
        $this->accessKey = $accessKey;
    }

    /**
     * {@inheritDoc}
     */
    public function generateSignature(): string
    {
        return hash(self::ALGORITHM, "{$this->apiKey},{$this->accessKey},{$this->timestamp}");
    }

    /**
     * {@inheritDoc}
     */
    public function getTimestamp(): string
    {
        return $this->timestamp;
    }

    /**
     * {@inheritDoc}
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * {@inheritDoc}
     */
    public function getAccessKey(): string
    {
        return $this->accessKey;
    }


}