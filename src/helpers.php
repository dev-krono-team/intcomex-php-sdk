<?php

if (!function_exists('invokeMethod')) {

    /**
     * Use Reflection class to invoke arbitrarily a method
     *
     * @param mixed $object Class instance
     * @param string $method Method to be executed
     * @param array $args Array of necessary arguments to execute the method
     * @return mixed
     */
    function invokeMethod(&$object, string $method, array $args = []) {
        try {
            $reflection = new ReflectionClass(get_class($object));
            $method = $reflection->getMethod($method);
            $method->setAccessible(true);
            return $method->invokeArgs($object, $args);
        } catch (ReflectionException $re) {
            die($re->getMessage());
        }
    }
}

if (!function_exists('arrayToXml')) {
    function arrayToXml(array $bodyContent)
    {
        $xml = new SimpleXMLElement('<Root/>');
        addToNode($bodyContent, $xml);
        return $xml->asXML();
    }
}

if (!function_exists('addToNode')) {
    function addToNode(
        $content,
        SimpleXMLElement $node,
        SimpleXMLElement $parentNode = null
    ) {
        if (is_array($content)) {
            foreach ($content as $argument => $value) {
                if (0 === $argument) {
                    $newNode = $node;
                } elseif (is_numeric($argument) && null !== $parentNode) {
                    $newNode = $parentNode->addChild($node->getName());
                } else {
                    $newNode = $node->addChild($argument);
                }
                addToNode($value, $newNode, $node);
            }
        } else {
            $node = dom_import_simplexml($node);
            $cnode = $node->ownerDocument;
            if (preg_match('/[\W]+/', $content)) {
                $node->appendChild($cnode->createCDATASection($content));
            } else {
                $node->nodeValue = htmlentities($content);
            }
        }
    }
}
