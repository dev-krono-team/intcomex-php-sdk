# Intcomex PHP sdk

This library provides a basics methods for consume [IWS](https://iws.intcomex.com/reference/api.html).
Actually supports all endpoint for orders and products...

## Requirements

* PHP ^7.*
* libcurl

## Installation

* Add this repo in your composer repositories section.
* Add kronoapp/intcomex-php-sdk : v1.2.0 in your composer require section.
* Finally run...

```
composer install
```

## Usage

### Get Catalog
```
<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Kronoapp\Security\Signature;
use Kronoapp\Service\CurlService;
use Kronoapp\Client\IntcomexClient;

DEFINE('API_KEY', '{YOUR_PUBLIC_KEY}');
DEFINE('ACCESS_KEY', '{YOUR_PRIVATE_KEY}');

$signature = new Signature(API_KEY, ACCESS_KEY, new DateTime());
$service = new CurlService();
$client = new IntcomexClient($signature, $service);

# All catalog with default filters
$catalog = $client->getCatalog('es');

# Catalog with optional filters
# Notice that we can add query parameter with key:value format into string
// $catalog = $client->getCatalog('es', 'includePriceData:false', 'inventoryFilter:InStock');
```

### Get single product

```
<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Kronoapp\Security\Signature;
use Kronoapp\Service\CurlService;
use Kronoapp\Client\IntcomexClient;

DEFINE('API_KEY', '{YOUR_PUBLIC_KEY}');
DEFINE('ACCESS_KEY', '{YOUR_PRIVATE_KEY}');

$signature = new Signature(API_KEY, ACCESS_KEY, new DateTime());
$service = new CurlService();
$client = new IntcomexClient($signature, $service);

# Get a single product.
# Locale and Sku are required
$product = $client->getProduct('es', '{YOUR_INTCOMEX_SKU}');

# Add filters
# Notice that we can add query parameter with key:value format into string
// $product = $client->getProduct('es', '{YOUR_INTCOMEX_SKU}', 'includePriceData:false');
// $product = $client->getProduct('es', '{YOUR_INTCOMEX_SKU}', 'includeInventoryData:false');
// $product = $client->getProduct('es', '{YOUR_INTCOMEX_SKU}', 'includePriceData:false', 'includeInventoryData:false');
```

For more info please visit [IWS] (https://iws.intcomex.com/reference/api.html)

## Contributing

This library use PSR coding styles.

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Author
Elias Camilo Martinez Salcedo [camilo300792](https://github.com/camilo300792).

Web developer [Kronotime SAS](https://kronotime.com.co/)
